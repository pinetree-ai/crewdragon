t = getprop("/sim/time/elapsed")

if t > 30 then
	setprop("/control/manual/rcs5",0)
	setprop("/control/manual/rcs13",0)
	
elseif t > 25 then
	setprop("/control/manual/rcs5",110)
	setprop("/control/manual/rcs13",110)

elseif t > 15 then
	setprop("/control/manual/rcs3",0)
	setprop("/control/manual/rcs6",0)
	setprop("/control/manual/rcs11",0)
	setprop("/control/manual/rcs14",0)

elseif t > 10 then
	setprop("/control/manual/rcs3",110)
	setprop("/control/manual/rcs6",110)
	setprop("/control/manual/rcs11",110)
	setprop("/control/manual/rcs14",110)

elseif t > 5 then
	setprop("/control/manual/rcs4",0)
	setprop("/control/manual/rcs12",0)

elseif t > 0 then
	setprop("/control/manual/rcs4",110)
	setprop("/control/manual/rcs12",110)
end