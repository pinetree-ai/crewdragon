#include "Dynamix.hxx"

#define PI 3.14159265358979323846

// NOTE: The armadillo linear algebra library is available. Use it!!! :)

// Output: U (control inputs)
// Inputs: Ts: sampling time
//		   X [gamma; gammad]
//		   U (control inputs) - used to get variable structure
//		   params (loaded parameters) - access with params["<param name>"]

double rcs_thrust, rcs_threshold;

// Desired linear and angular velocities
double Udes, Vdes, Wdes, Pdes, Qdes, Rdes;

int control_axis_cycle = 0; // Each control axis gets one cycle "turn" to run in the controller.
// Cycles 0 (U), 1 (V), 2 (W), 3 (P), 4 (Q), 5 (R)

// Helper funciton to cutoff thrusters
void cutoff_thrusters(map<string, double> &U)
{
	for (int i=1; i<=16; i++)
		U["rcs" + to_string(i)] = 0;
}

map<string, double> controller_cxx(double Ts, vec X, map<string, double> U, map<string, double> params)
{
	rcs_thrust = params["rcs_thrust"];
	rcs_threshold = params["rcs_threshold"];

	// Check RCS control mode (manual or auto)
	if (PropertyTree::props()->getString("/control/rcs-mode").compare("auto") == 0) {
		// Automatic control mode

		// Get desired linear and angular velocity vectors
		Udes = PropertyTree::props()->getDouble("/control/auto/U");
		Vdes = PropertyTree::props()->getDouble("/control/auto/V");
		Wdes = PropertyTree::props()->getDouble("/control/auto/W");

		Pdes = PropertyTree::props()->getDouble("/control/auto/P");
		Qdes = PropertyTree::props()->getDouble("/control/auto/Q");
		Rdes = PropertyTree::props()->getDouble("/control/auto/R");

		switch (control_axis_cycle) {
			case 0: // U
				if (Udes > rcs_threshold) {
					U["rcs3"] = rcs_thrust;
					U["rcs4"] = rcs_thrust;
					U["rcs13"] = rcs_thrust;
					U["rcs14"] = rcs_thrust;
				}
				else if (Udes < -rcs_threshold) {
					U["rcs5"] = rcs_thrust;
					U["rcs6"] = rcs_thrust;
					U["rcs11"] = rcs_thrust;
					U["rcs12"] = rcs_thrust;
				}
				else cutoff_thrusters(U);

				break;
			
			case 1: // V
				if (Vdes > rcs_threshold) {
					U["rcs3"] = rcs_thrust;
					U["rcs4"] = rcs_thrust;
					U["rcs5"] = rcs_thrust;
					U["rcs6"] = rcs_thrust;
				}
				else if (Vdes < -rcs_threshold) {
					U["rcs11"] = rcs_thrust;
					U["rcs12"] = rcs_thrust;
					U["rcs13"] = rcs_thrust;
					U["rcs14"] = rcs_thrust;
				}
				else cutoff_thrusters(U);

				break;
			
			case 2: // W
				if (Wdes > rcs_threshold) {
					U["rcs4"] = rcs_thrust;
					U["rcs5"] = rcs_thrust;
					U["rcs12"] = rcs_thrust;
					U["rcs13"] = rcs_thrust;
				}
				else if (Wdes < -rcs_threshold) {
					U["rcs1"] = rcs_thrust;
					U["rcs2"] = rcs_thrust;
					U["rcs7"] = rcs_thrust;
					U["rcs8"] = rcs_thrust;
					U["rcs9"] = rcs_thrust;
					U["rcs10"] = rcs_thrust;
					U["rcs15"] = rcs_thrust;
					U["rcs16"] = rcs_thrust;
				}
				else cutoff_thrusters(U);

				break;
			
			case 3: // P

				if (Pdes > rcs_threshold) {
					U["rcs2"] = rcs_thrust;
					U["rcs7"] = rcs_thrust;
					U["rcs12"] = rcs_thrust;
					U["rcs13"] = rcs_thrust;
				}
				else if (Pdes < -rcs_threshold) {
					U["rcs10"] = rcs_thrust;
					U["rcs15"] = rcs_thrust;
					U["rcs5"] = rcs_thrust;
					U["rcs4"] = rcs_thrust;
				}
				else cutoff_thrusters(U);

				break;
			
			case 4: // Q

				if (Qdes > rcs_threshold) {
					U["rcs8"] = rcs_thrust;
					U["rcs9"] = rcs_thrust;
					U["rcs4"] = rcs_thrust;
					U["rcs13"] = rcs_thrust;
				}
				else if (Qdes < -rcs_threshold) {
					U["rcs1"] = rcs_thrust;
					U["rcs16"] = rcs_thrust;
					U["rcs5"] = rcs_thrust;
					U["rcs12"] = rcs_thrust;
				}
				else cutoff_thrusters(U);

				break;
			
			case 5: // R
				if (Rdes > rcs_threshold) {
					U["rcs3"] = rcs_thrust;
					U["rcs4"] = rcs_thrust;
					U["rcs11"] = rcs_thrust;
					U["rcs12"] = rcs_thrust;
				}
				else if (Rdes < -rcs_threshold) {
					U["rcs5"] = rcs_thrust;
					U["rcs6"] = rcs_thrust;
					U["rcs13"] = rcs_thrust;
					U["rcs14"] = rcs_thrust;
				}
				else cutoff_thrusters(U);

				break;
		}

		// Manage control cycle
		control_axis_cycle++;
		if (control_axis_cycle > 5) control_axis_cycle = 0; // Reset cycle
	
	} else {
		// Manual control mode

		for (int i=1; i<=16; i++)
			U["rcs" + to_string(i)] = PropertyTree::props()->getDouble("/control/manual/rcs" + to_string(i));
	}

	// Control law: compute ideal RCS outputs to achieve Fdes and Mdes

	// Set animation properties
	for (int i=1; i<=16; i++)
		PropertyTree::props()->set("/rcs/thruster" + to_string(i) + "/norm", U["rcs" + to_string(i)]/rcs_thrust - 1);

	return U;
}
