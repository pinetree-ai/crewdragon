-- Constants
vdes = 0.1
wdes = 0.1

if getprop("/keyboard/state") == 1 then

    key = getprop("/keyboard/key")

    -- Translational control
    if key == 101 then -- Z+
        setprop("/control/auto/W", vdes)
    
    elseif key == 113 then -- Z-
        setprop("/control/auto/W", -vdes)

    elseif key == 119 then -- X+
        setprop("/control/auto/U", vdes)

    elseif key == 115 then -- X-
        setprop("/control/auto/U", -vdes)

    elseif key == 97 then -- Y+
        setprop("/control/auto/V", vdes)

    elseif key == 100 then -- Y-
        setprop("/control/auto/V", -vdes)

    -- Rotational control
    elseif key == 111 then -- Yaw right
        setprop("/control/auto/R", -wdes)
    
    elseif key == 117 then -- Yaw left
        setprop("/control/auto/R", wdes)

    elseif key == 107 then -- Pitch up
        setprop("/control/auto/Q", wdes)

    elseif key == 105 then -- Pitch down
        setprop("/control/auto/Q", -wdes)

    elseif key == 108 then -- Roll right
        setprop("/control/auto/P", wdes)

    elseif key == 106 then -- Roll left
        setprop("/control/auto/P", -wdes)

    end
    
else

    -- Reset to zero target velocities
    setprop("/control/auto/U", 0)
    setprop("/control/auto/V", 0)
    setprop("/control/auto/W", 0)
    setprop("/control/auto/P", 0)
    setprop("/control/auto/Q", 0)
    setprop("/control/auto/R", 0)

end