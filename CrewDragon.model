# Dynamix Model Definition for the SpaceX Dragon V2 Crew Module
# Authors: Narendran Muraleedharan <narendran.m@aptus.aero>
#
# COPYRIGHT (c) 2019 Pinetree LLC & Aptus Engineering, Inc.
#
# This project is distributed as a sample model with the Dynamix.0
# dyamic simulation suite.
#
# This project is an example to demonstrate multi-body systems

# Define state variables - initial conditions
# cX generalized coordinates are for the crew capsule
# tX generalized coordinates are for the trunk capsule
GAMMA:			cx=0, cy=0, cz=0, cphi=0, ctheta=0, cpsi=0, tx=0, ty=0, tz=0, tphi=0, ttheta=0, tpsi=0

# Define state velocities - initial conditions
GAMMA_DOT:		cxd=0, cyd=0, czd=0, cphid=0, cthetad=0, cpsid=0, txd=0, tyd=0, tzd=0, tphid=0, tthetad=0, tpsid=0

# Define input variables - initial conditions
INPUTS:			rcs1=0, rcs2=0, rcs3=0, rcs4=0, rcs5=0, rcs6=0, rcs7=0, rcs8=0, rcs9=0, rcs10=0, rcs11=0, rcs12=0, rcs13=0, rcs14=0, rcs15=0, rcs16=0

# Set a *.cxx filepath to use custom hard-coded dynamics - write the
#  model dynamics as dynamics.cxx in the model root directory.
# Set a *.dmx filepath to use the dynamix-defined model
DYN_PATH:		dynamics.cxx

# Set a *.cxx filepath to use a customer hard-coded controller - write
#  the controller as controller.cxx in the model root directory.
# Set a *.lua or *.py file to use a script as the primary controller.
# NOTE: To use a combination of scripting and a hard-coded controller,
#  define the script below in the SCRIPTS path variable and set
#  properties in the tree which can be accessed by controller.cxx. Then,
#  define the controller path as the *.cxx file.
CTL_PATH:		control/rcs.cxx

# Define model file paths
MODEL_PATH:		3d/dragon.ac
ANIM_PATH:		animations.dmx
PARAMS_PATH:	parameters.dmx

# Run-time script paths
# SCRIPTS:		control/rcstest.lua
SCRIPTS:        control/keyboard.lua

# Configuration parmeters
# Background color
CFG:BGCOLOR:    0.0,0.05,0.1

# Properties
# Start with trunk latched to capsule
PROP:/system/trunk-state:latched

# Start with RCS in auto control mode
PROP:/control/rcs-mode:auto

# Initial desired linear and angular velocity values
# Linear velocities: {U,V,W}
PROP:/control/auto/U:0
PROP:/control/auto/V:0
PROP:/control/auto/W:0
# Rotation rates: {P,Q,R}
PROP:/control/auto/P:0
PROP:/control/auto/Q:0
PROP:/control/auto/R:0