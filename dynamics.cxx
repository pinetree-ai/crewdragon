#include "Dynamix.hxx"

#define PI 3.14159265358979323846
#define GAMMA_LEN 12

// This project is an example of a simple multi-body system (2 bodies)
// In this example, the 2 bodies (capsule and trunk) can either be latched together or separated. Depending on the property, we will compute the dynamics as either a grouped model or separated.

// This project is also a good example of how the RigidBody class is used
// See https://aptus.aero/pinetree/reference/dynamix/index.php?p=C%2B%2B/Dynamics/RigidBody


// NOTE: The armadillo linear algebra library is available. Use it!!! :)

// Output: Xdot [gammad, gammadd]
// Inputs: X [gamma, gammad]
//		   U (control inputs)
//		   params (loaded parameters) - access with params["<param name>"]

// Variables
vec Xdot(GAMMA_LEN * 2); // Return state velocities

vec Frcs(3), Rrcs(3); // Individual forces and arms for RCS thrusters

// Generalized coordinates, velocities and accelerations
vec q(6);
vec qd(6);
vec qdd(6);

// Rigid Body objects
RigidBody *capsule = NULL;
RigidBody *combined = NULL;

RigidBody *selected = NULL; // Pointer to selected rigidbody object (capsule or combined)

// NOTE: We're using the aerospace standard ZYX Euler rotation sequence

vec dynamics_cxx(vec X, map<string, double> U, map<string, double> params, map<string, itable> tables)
{
	// Initialize bodies
	if (capsule == NULL || combined == NULL) {
		// Capsule
		vec cm(3); // Center of mass
		cm.zeros();
		cm(2) = params["capsule_CMz"];

		double m = params["capsule_m"];

		mat J(3,3); // Inertia tensor
		J.zeros();
		J(0,0) = params["capsule_JXY"] + m * params["capsule_CMz"] * params["capsule_CMz"];
		J(1,1) = J(0,0); // Axi-symmetric
		J(2,2) = params["capsule_Jzz"];

		capsule = new RigidBody(m, cm, J);

		// Make modifications for combined body
		cm(2) = (cm(2) * m + params["trunk_CMz"] * params["trunk_m"]);

		m += params["trunk_m"];

		cm(2) /= m; // Because we haven't done that yet

		// Compute combined inertia tensor
		// X and Y axes
		J(0,0) += params["trunk_JXY"] + params["trunk_m"] * params["trunk_CMz"] * params["trunk_CMz"];
		J(1,1) = J(0,0); // Axi-symmetric
		J(2,2) += params["trunk_Jzz"];

		combined = new RigidBody(m, cm, J);

		// Default g vector is zeros
	}

	// The property /system/trunk-state can be "latched" or "free"
	if (PropertyTree::props()->getString("/system/trunk-state").compare("latched") == 0)
		selected = combined;
	else
		selected = capsule;

	Xdot.zeros();

	for (int i=0; i<GAMMA_LEN; i++) {
		Xdot(i) = X(GAMMA_LEN+i);
	}

	// Get system state variables
	for (int i=0; i<6; i++) {
		q(i) = X(i);
		qd(i) = X(GAMMA_LEN+i);
	}

	selected->setq(q, qd);

	// Compute net forces and moments vectors due to RCS thrusters
	for (int i=1; i<=16; i++) {
		// the d params are the mass explusion directions
		// The net forces are in the opposite direction
		Frcs(0) = -1.0 * U["rcs" + to_string(i)] * params["rcs" + to_string(i) + "_dx"];
		Frcs(1) = -1.0 * U["rcs" + to_string(i)] * params["rcs" + to_string(i) + "_dy"];
		Frcs(2) = -1.0 * U["rcs" + to_string(i)] * params["rcs" + to_string(i) + "_dz"];
		
		selected->applyBodyForce(Frcs);

		// Forces and moments are about the center of mass of the capsule
		Rrcs(0) = params["rcs" + to_string(i) + "_cx"];
		Rrcs(1) = params["rcs" + to_string(i) + "_cy"];
		Rrcs(2) = params["rcs" + to_string(i) + "_cz"] - params["capsule_CMz"];

		// M = r x F, and add to net moments vector
		selected->applyBodyMoment(cross(Rrcs, Frcs));
	}

	// Simulate the dynamics as a single body
	qdd = selected->compute();

	for (int i=0; i<6; i++) {
		Xdot(GAMMA_LEN+i) = qdd(i);
		
		if (selected == combined) // Values are the same for the capsule and the trunk if combined
			Xdot(GAMMA_LEN+6+i) = qdd(i);
	}
	
	return Xdot;
}
